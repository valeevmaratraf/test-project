using UnityEngine;

using Zenject;

namespace TestProject
{
    public class ProjectInstaller : MonoInstaller<ProjectInstaller>
    {
        [SerializeField] private AppSettings appSettings;

        [SerializeField] private GameObject cardPrefab;

        public override void InstallBindings()
        {
            gameObject.name = "[SYSTEM]";

            Container.Bind<AppSettings>().FromInstance(appSettings).AsSingle();
            Container.Bind<ImageDownloader>().FromNewComponentOnNewGameObject().AsSingle();

            var cardControllerArgs = new HandCardsStack.ConstructArgs()
            {
                CardPrefab = cardPrefab
            };
            Container.Bind<HandCardsStack.ConstructArgs>().FromInstance(cardControllerArgs).AsSingle();
        }
    }
}