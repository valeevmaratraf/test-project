using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Zenject;

namespace TestProject
{
    public class SceneInstaller : MonoInstaller
    {
        [SerializeField] private Canvas mainCanvas;

        public override void InstallBindings()
        {
            Container.Bind<Canvas>().FromInstance(mainCanvas).AsSingle();
        }
    }
}
