using System.Collections;
using System.Collections.Generic;

using UniRx;

using UnityEngine;

namespace TestProject
{
    public class CardModel
    {
        public ReactiveProperty<int> Health;
        public ReactiveProperty<int> Attack;
        public ReactiveProperty<int> Cost;
        public ReactiveProperty<string> Name;
        public ReactiveProperty<string> Description;

        private ReactiveProperty<int>[] parameters;

        public CardModel(bool randomValues = true)
        {
            Health = new ReactiveProperty<int>();
            Attack = new ReactiveProperty<int>();
            Cost = new ReactiveProperty<int>();
            parameters = new ReactiveProperty<int>[] { Health, Attack, Cost };

            Name = new ReactiveProperty<string>();
            Description = new ReactiveProperty<string>();

            if (randomValues)
                Randomize();
        }

        public void Setup(CardData data)
        {
            Health.Value = data.Health;
            Attack.Value = data.Attack;
            Cost.Value = data.Cost;

            Name.Value = data.Name;
            Description.Value = data.Description;
        }

        public void Randomize()
        {
            Health.Value = Random.Range(1, 9);
            Attack.Value = Random.Range(1, 9);
            Cost.Value = Random.Range(1, 9);

            Name.Value = "Card #" + Random.Range(0, 100);
            Description.Value = "This is card description #" + Random.Range(0, 100);
        }

        public void RandomizeOneValue(int paramIndex)
        {
            parameters[paramIndex].SetValueAndForceNotify(Random.Range(-2, 9));
        }
    }

    public struct CardData
    {
        public int Health;
        public int Attack;
        public int Cost;
        public string Name;
        public string Description;
    }
}
