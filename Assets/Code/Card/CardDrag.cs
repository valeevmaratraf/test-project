using DG.Tweening;

using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TestProject
{
    public class CardDrag : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        [SerializeField] private Card card;
        [SerializeField] private Image glowEffect;

        private Tween glowAnim;

        private bool isDragging;

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (card.IsCanBeSelected)
            {
                isDragging = true;
                card.StartDrag();

                SetGlowEffect(true);

                glowAnim = glowEffect.DOFade(0.9F, 0.5F);
                glowAnim.onComplete += () => glowAnim = null;
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (isDragging)
            {
                Vector3 targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                targetPos.z = 5;
                card.SetDragPosition(targetPos);
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (isDragging)
            {
                SetGlowEffect(false);

                List<RaycastResult> results = new List<RaycastResult>(8);
                EventSystem.current.RaycastAll(eventData, results);
                foreach (var result in results)
                {
                    var holder = result.gameObject.GetComponent<ICardHolder>();
                    if (holder != null)
                    {
                        holder.AddCard(card);
                        isDragging = false;
                        return;
                    }
                }

                card.CancelDrag();

                isDragging = false;
            }
        }

        private void SetGlowEffect(bool isOn)
        {
            if (glowAnim != null)
                glowAnim.Kill();

            float targetValue = isOn ? 0.9F : 0;
            glowAnim = glowEffect.DOFade(targetValue, 0.5F);
            glowAnim.onComplete += () => glowAnim = null;
        }

        private void OnDestroy()
        {
            if (glowAnim != null)
                glowAnim.Kill();
        }
    }
}
