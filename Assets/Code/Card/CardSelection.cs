using UnityEngine;
using UnityEngine.EventSystems;

using Zenject;

namespace TestProject
{
    public class CardSelection : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private Card card;
        [SerializeField] private RectTransform cardVisualRT;

        private AppSettings appSettings;
        private bool isMouseOnCard;

        private void Awake()
        {
            card.MoveEnd += () => SetSelection(isMouseOnCard);
        }

        [Inject]
        private void Construct(AppSettings appSettings)
        {
            this.appSettings = appSettings;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            isMouseOnCard = true;
            SetSelection(isMouseOnCard);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            isMouseOnCard = false;
            SetSelection(isMouseOnCard);
        }

        private void SetSelection(bool value)
        {
            if (!card.IsCanBeSelected)
                return;

            var targetPos = Vector2.zero;
            if (value)
                targetPos = cardVisualRT.InverseTransformDirection(cardVisualRT.up * appSettings.CardSelectionMoveOffset);

            card.MoveVisual(targetPos);
        }
    }
}
