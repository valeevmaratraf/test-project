using DG.Tweening;

using System;

using TMPro;

using UniRx;

using UnityEngine;
using UnityEngine.UI;

using Zenject;

namespace TestProject
{
    [RequireComponent(typeof(CardUI))]
    public class Card : MonoBehaviour
    {
        [SerializeField] private RectTransform cardVisualRT;

        private CardUI ui;
        private CardModel model;

        private AppSettings appSettings;
        private Canvas mainCanvas;
        private RectTransform cardRT;

        private Tween moveAnim;
        private Tween moveVisualAnim;
        private Tween rotationAnim;

        private ICardHolder lastHolder;
        private ICardHolder currentHolder;

        public event Action MoveEnd;
        public event Action<Card> Disconnected;
        public event Action<Card> OnDestroyed;

        public bool IsCanBeSelected => currentHolder != null && currentHolder.IsCardsCanDisconnect;

        [Inject]
        private void Construct(AppSettings appSettings, Canvas mainCanvas)
        {
            cardRT = GetComponent<RectTransform>();
            this.appSettings = appSettings;
            this.mainCanvas = mainCanvas;

            model = new CardModel();
            ui = GetComponent<CardUI>();
            ui.AttachToModel(model);
        }

        public void SetCurrentCardHolder(ICardHolder holder, Vector3 position, Quaternion rotation, Transform parent)
        {
            cardRT.SetParent(parent, true);
            currentHolder = holder;
            lastHolder = null;
            Move(position);

            if (rotationAnim != null)
                rotationAnim.Kill();

            rotationAnim = cardRT.DORotateQuaternion(rotation, appSettings.CardAutoMoveTime);
            rotationAnim.onComplete += () => rotationAnim = null;
        }

        private void Move(Vector3 newPosition, bool resetVisualPosition = true)
        {
            if (moveAnim != null)
                moveAnim.Kill();

            if (resetVisualPosition)
                MoveVisual(Vector3.zero);

            moveAnim = cardRT.DOLocalMove(newPosition, appSettings.CardAutoMoveTime);
            moveAnim.onComplete += () =>
            {
                MoveEnd?.Invoke();
                moveAnim = null;
            };
        }

        public void MoveVisual(Vector3 newPosition)
        {
            if (moveVisualAnim != null)
                moveVisualAnim.Kill();

            moveVisualAnim = cardVisualRT.DOLocalMove(newPosition, appSettings.CardAutoMoveTime);
            moveVisualAnim.onComplete += () => moveVisualAnim = null;
        }

        public void StartDrag()
        {
            if (!IsCanBeSelected)
            {
                Debug.Log("This card cant disconnect: IsCanBeSelected = false.");
                return;
            }

            if (moveAnim != null)
            {
                moveAnim.Kill();
                moveAnim = null;
            }

            MoveVisual(Vector2.zero);

            lastHolder = currentHolder;
            currentHolder = null;

            cardRT.SetParent(mainCanvas.transform, true);
            Disconnected?.Invoke(this);

            if (rotationAnim != null)
                rotationAnim.Kill();
            rotationAnim = cardRT.DORotateQuaternion(Quaternion.identity, appSettings.CardAutoMoveTime);
            rotationAnim.onComplete += () => rotationAnim = null;
        }

        public void CancelDrag()
        {
            lastHolder.AddCard(this);
        }

        public void SetDragPosition(Vector3 position, Space space = Space.World)
        {
            if (space == Space.World)
                cardRT.position = position;
            else
                cardRT.localPosition = position;
        }

        public void RandomizeRandomParam(int parameterIndex, Action animationCompleteCallback = null)
        {
            animationCompleteCallback += () => ui.LerpComplete -= animationCompleteCallback;
            ui.LerpComplete += animationCompleteCallback;
            model.RandomizeOneValue(parameterIndex);
        }

        public void CheckHealth()
        {
            if (model.Health.Value <= 0)
                Destroy(gameObject);
        }

        private void OnDestroy()
        {
            OnDestroyed?.Invoke(this);

            transform.DOKill();
            cardVisualRT.DOKill();
        }
    }
}
