using System;
using System.Collections.Generic;

using TMPro;

using UniRx;

using UnityEngine;
using UnityEngine.UI;

using Zenject;

namespace TestProject
{
    public class CardUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI attackTMP;
        [SerializeField] private TextMeshProUGUI healthTMP;
        [SerializeField] private TextMeshProUGUI costTMP;

        [SerializeField] private TextMeshProUGUI titleTMP;
        [SerializeField] private TextMeshProUGUI descriptionTMP;

        [SerializeField] private Image portrait;

        private bool isLerpersInitialized;

        private ImageDownloader imageDownloader;

        private ParameterLerper attackLerper, healthLerper, costLerper;

        public event Action LerpComplete;

        [Inject]
        private void Construct(ImageDownloader imageDownloader)
        {
            this.imageDownloader = imageDownloader;
            if (!isLerpersInitialized)
                InitializeLerpers();
        }

        private void InitializeLerpers()
        {
            attackLerper = new ParameterLerper(attackTMP);
            healthLerper = new ParameterLerper(healthTMP);
            costLerper = new ParameterLerper(costTMP);

            attackLerper.ValueChangeComplete += () => LerpComplete?.Invoke();
            healthLerper.ValueChangeComplete += () => LerpComplete?.Invoke();
            costLerper.ValueChangeComplete += () => LerpComplete?.Invoke();

            isLerpersInitialized = true;
        }

        private void Start()
        {
            imageDownloader.LoadRandomImage((image) =>
            {
                portrait.sprite = Sprite.Create(image, new Rect(Vector2.zero, new Vector2(image.width, image.height)), Vector2.one / 2);
            });
        }

        public void AttachToModel(CardModel model)
        {
            if (!isLerpersInitialized)
                InitializeLerpers();

            model.Name.Subscribe(value => titleTMP.text = value);
            model.Description.Subscribe(value => descriptionTMP.text = value);
            model.Attack.Subscribe(value => attackLerper.SetTargetValue(value));
            model.Health.Subscribe(value => healthLerper.SetTargetValue(value));
            model.Cost.Subscribe(value => costLerper.SetTargetValue(value));
        }

        public void StopLerps()
        {
            attackLerper.StopLerp();
            healthLerper.StopLerp();
            costLerper.StopLerp();
        }

        private void OnDestroy()
        {
            StopLerps();
        }
    }
}
