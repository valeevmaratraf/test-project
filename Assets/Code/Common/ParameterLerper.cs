﻿using DG.Tweening;

using System;

using TMPro;

using UniRx;

using UnityEngine;

namespace TestProject
{
    public class ParameterLerper
    {
        private TextMeshProUGUI ui;
        private Tween currentLerp;

        public event Action ValueChangeComplete;

        public ParameterLerper(TextMeshProUGUI ui)
        {
            this.ui = ui;
        }

        public void SetTargetValue(int targetValue)
        {
            if (int.TryParse(ui.text, out int currentValue))
            {
                StopLerp();

                var changer = new ReactiveProperty<int>(currentValue);
                currentLerp = DOTween.To(() => changer.Value, (value) => changer.Value = value, targetValue, 1);
                currentLerp.onComplete += () =>
                {
                    currentLerp = null;
                    ValueChangeComplete?.Invoke();
                    changer.Dispose();
                };

                changer.Subscribe((value) =>
                {
                    ui.text = value.ToString();
                });
            }
            else
                Debug.LogError("Unexcectable value");
        }

        public void StopLerp()
        {
            if (currentLerp != null)
            {
                currentLerp.Kill();
                currentLerp = null;
            }
        }
    }
}
