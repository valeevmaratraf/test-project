using UnityEngine;

namespace TestProject
{
    [CreateAssetMenu(fileName = "AppSettings", menuName = "Custom assets/App settings")]
    public class AppSettings : ScriptableObject
    {
        [field: SerializeField] public string ImageSourceURL { get; private set; } = "https://picsum.photos";
        [field: SerializeField] public Vector2 ImageSize { get; private set; } = new Vector2(100, 100);

        public int CardsCount => Random.Range(4, 6);
        [field: SerializeField] public float CardsArcRadius { get; private set; } = 100F;
        [field: SerializeField] public float StartCardsStackAngle { get; private set; } = -90;
        [field: SerializeField] public float EndCardsStackAngle { get; private set; } = 90;

        [field: SerializeField] public float CardSelectionMoveOffset { get; private set; } = 90;
        [field: SerializeField] public float CardAutoMoveTime { get; private set; } = 1F;

        [field: SerializeField] public float CardsTableOffset { get; private set; } = 100;
    }
}
