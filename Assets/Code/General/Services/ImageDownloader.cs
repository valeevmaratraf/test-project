using System;
using System.Collections;

using UnityEngine;
using UnityEngine.Networking;

using Zenject;

namespace TestProject
{
    public class ImageDownloader : MonoBehaviour
    {
        private AppSettings appSettings;

        [Inject]
        private void Construct(AppSettings appSettings)
        {
            this.appSettings = appSettings;
        }

        public void LoadRandomImage(Action<Texture2D> callback)
        {
            string url = $"{ appSettings.ImageSourceURL }/{ appSettings.ImageSize.x }/{ appSettings.ImageSize.y }";
            StartCoroutine(ImageLoader(url, callback));
        }

        private IEnumerator ImageLoader(string url, Action<Texture2D> callback)
        {
            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url))
            {
                yield return uwr.SendWebRequest();

                if (uwr.result != UnityWebRequest.Result.Success)
                {
                    Debug.Log(uwr.error);
                }
                else
                {
                    callback?.Invoke(DownloadHandlerTexture.GetContent(uwr));
                }
            }
        }
    }
}
