using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace TestProject
{
    [RequireComponent(typeof(Button))]
    public class CardsParamsChanger : MonoBehaviour
    {
        [SerializeField] private HandCardsStack cardsCreator;

        private Coroutine randomizeProcess;

        private void Awake()
        {
            GetComponent<Button>().onClick.AddListener(ButtonClickHandler);
        }

        private void ButtonClickHandler()
        {
            if (randomizeProcess != null)
                return;

            randomizeProcess = StartCoroutine(SequenceRoutine());
        }

        private IEnumerator SequenceRoutine()
        {
            cardsCreator.IsCardsCanDisconnect = false;

            int cardsCounter = 0;
            bool next = true;
            int direction = 1;
            var paramNumber = Random.Range(0, 3);
            var cards = cardsCreator.Cards;

            while (cardsCounter < cards.Count && direction > 0 || cardsCounter >= 0 && direction < 0)
            {
                var card = cards[cardsCounter];
                if (next)
                {
                    next = false;

                    cardsCounter += direction;

                    card.RandomizeRandomParam(paramNumber, () => next = true);
                }

                if (cardsCounter >= cards.Count)
                {
                    direction = -1;
                    cardsCounter--;
                }

                yield return new WaitForEndOfFrame();
            }


            for (int i = cards.Count - 1; i >= 0; i--)
            {
                Card card = cards[i];
                card.CheckHealth();
            }

            randomizeProcess = null;
            cardsCreator.IsCardsCanDisconnect = true;
        }
    }
}
