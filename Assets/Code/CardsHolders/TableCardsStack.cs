using System.Collections.Generic;

using UnityEngine;

using Zenject;

namespace TestProject
{
    public class TableCardsStack : MonoBehaviour, ICardHolder
    {
        private AppSettings appSettings;

        private List<Card> cards;

        public bool IsCardsCanDisconnect => false;

        [Inject]
        private void Construct(AppSettings appSettings)
        {
            this.appSettings = appSettings;
            cards = new List<Card>(8);
        }

        public void AddCard(Card card)
        {
            cards.Add(card);
            card.OnDestroyed += CardDestroyHandler;
            SetCardsPositions();
        }

        private void SetCardsPositions()
        {
            float xStart = -appSettings.CardsTableOffset * (cards.Count - 1) / 2F;
            for (int i = 0; i < cards.Count; i++)
            {
                Card card = cards[i];
                float cardPosX = xStart + i * appSettings.CardsTableOffset;
                Vector3 cardPos = new Vector3(cardPosX, 0);
                card.SetCurrentCardHolder(this, cardPos, Quaternion.identity, transform);
            }
        }

        private void CardDestroyHandler(Card sender)
        {
            cards.Remove(sender);
            SetCardsPositions();
        }
    }
}
