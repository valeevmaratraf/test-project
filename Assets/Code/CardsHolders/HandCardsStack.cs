using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Zenject;

namespace TestProject
{
    public class HandCardsStack : MonoBehaviour, ICardHolder
    {
        private AppSettings appSettings;
        private GameObject cardPrefab;

        private List<Card> cards;
        public bool IsCardsCanDisconnect { get; set; }

        public event Action<Card> CardCreated;

        public IReadOnlyList<Card> Cards => cards.AsReadOnly();

        [Inject]
        private void Construct(AppSettings appSettings, ConstructArgs cardPrefab)
        {
            this.appSettings = appSettings;
            this.cardPrefab = cardPrefab.CardPrefab;
            IsCardsCanDisconnect = true;
        }

        private void Start()
        {
            cards = new List<Card>(appSettings.CardsCount);

            for (int i = 0; i < appSettings.CardsCount; i++)
                CreateCard(i);

            SetCardsPoses();
        }

        private void SetCardsPoses()
        {
            if (cards.Count == 0)
                return;

            if (cards.Count == 1)
            {
                cards[0].SetCurrentCardHolder(this, Vector3.up * appSettings.CardsArcRadius,
                    Quaternion.identity, transform);
                return;
            }

            float deltaAngle = (-appSettings.StartCardsStackAngle + appSettings.EndCardsStackAngle) / (cards.Count - 1);
            float startAngle = appSettings.StartCardsStackAngle;
            for (int i = 0; i < cards.Count; i++)
            {
                float angle = -startAngle - deltaAngle * i;
                Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                Vector3 upDir = rotation * Vector3.up;
                Vector3 cardPos = upDir * appSettings.CardsArcRadius;

                Card card = cards[i];
                card.SetCurrentCardHolder(this, cardPos, rotation, transform);
            }
        }

        private void CreateCard(int number)
        {
            var cardGO = Instantiate(cardPrefab);
            cardGO.name = $"Card_{ number }";

            var cardRT = cardGO.GetComponent<RectTransform>();
            cardRT.SetParent(transform);

            cardRT.anchorMax = cardRT.anchorMin = Vector3.one / 2;
            cardRT.anchoredPosition = Vector3.zero;

            cardRT.localScale = Vector3.one;

            var card = cardGO.GetComponent<Card>();

            card.Disconnected += CardDisconnectHandler;
            card.OnDestroyed += CardDestroyHandler;

            cards.Add(card);

            CardCreated?.Invoke(card);
        }

        private void CardDisconnectHandler(Card sender)
        {
            sender.Disconnected -= CardDisconnectHandler;
            sender.OnDestroyed -= CardDestroyHandler;

            cards.Remove(sender);
            SetCardsPoses();
        }

        public void AddCard(Card card)
        {
            card.Disconnected += CardDisconnectHandler;
            card.OnDestroyed += CardDestroyHandler;

            cards.Add(card);
            SetCardsPoses();
        }

        private void CardDestroyHandler(Card sender)
        {
            cards.Remove(sender);
            SetCardsPoses();
        }

        public struct ConstructArgs
        {
            public GameObject CardPrefab;
        }
    }
}
