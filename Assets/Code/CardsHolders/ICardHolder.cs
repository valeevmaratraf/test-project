﻿namespace TestProject
{
    public interface ICardHolder
    {
        public void AddCard(Card card);
        public bool IsCardsCanDisconnect { get; }
    }
}